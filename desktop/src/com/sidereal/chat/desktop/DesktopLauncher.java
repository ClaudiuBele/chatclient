package com.sidereal.chat.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.core.DolphinOESConfiguration;
import com.sidereal.splash.SplashScreen;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
		config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;

		DolphinOESConfiguration dconfig = new DolphinOESConfiguration();
		dconfig.inputProcessorNames = new String[]{"Overlay","UI","Main"};
		dconfig.debugEnabled = true;
		
		new LwjglApplication(new DolphinOES(new SplashScreen(),dconfig), config);
		
		
	}
}
