package com.sidereal.splash;

import com.badlogic.gdx.Gdx;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.ui.TextBuilder;

public class Title extends TextBuilder
{

	public Title(GameScene scene)
	{
		super(scene, false, "Calibri.fnt");
		
		// will change through scenes
		isPersistent = true;
		setAnchor(Anchor.Top);
		setName("Title");
		setType("UI");
		
		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		super.onResize(x, y, oldX, oldY);
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		
		setScale(smallestSize/1000f);
		position.set(x/2f, y*0.975f);
	}

}
