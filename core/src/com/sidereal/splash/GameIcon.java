package com.sidereal.splash;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.texture.TextureBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.texture.TextureDrawer;

public class GameIcon extends GameObject
{

	private Renderer renderer;
	
	public GameIcon(GameScene scene, Object[] params)
	{
		super(scene, params);
	}
	
	@Override
	protected void onCreate(Object... params)
	{
		super.onCreate(params);
		
		renderer = new Renderer(this);

		
		float screenSizeToCalculateFor = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		float width =  screenSizeToCalculateFor * 0.6f;
		float height = screenSizeToCalculateFor * 0.45f;
		
		TextureBuilder builder = new TextureBuilder("sigla_v1_black_full.png")
			.setSize(width , height)
			.setOffsetPosition(-width/2f, -height/2f);
		renderer.addDrawer("Main", builder);
		
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		super.onResize(x, y, oldX, oldY);
		float screenSizeToCalculateFor = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		float width =  screenSizeToCalculateFor * 0.6f;
		float height = screenSizeToCalculateFor * 0.45f;
		renderer.getDrawer("Main", TextureDrawer.class).setSize(new Vector2(width,height));
		renderer.getDrawer("Main", TextureDrawer.class).setOffsetPosition(new Vector2(-width/2f,-height/2f));

	}

}
