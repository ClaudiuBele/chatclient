package com.sidereal.splash;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader.BitmapFontParameter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.assetload.AssetLoader;
import com.sidereal.login.LoginScreen;

public class SplashScreen extends GameScene
{

	@Override
	public void createScene()
	{
		bgColor =Color.WHITE;
		
		AssetLoader loader = new AssetLoader(this, new LoginScreen());
		
		BitmapFontParameter bitmapSettings = new BitmapFontParameter();
		bitmapSettings.magFilter = TextureFilter.Linear;
		bitmapSettings.minFilter = TextureFilter.Linear;
		
		
		
		loader.load("background_9.png", Texture.class);
		loader.load("button_9.png"	, Texture.class);
		
		loader.load("sigla_v1_black_full.png", Texture.class);
		loader.load("spinner.png", Texture.class);
		
		loader.load("Blocks.fnt", BitmapFont.class, bitmapSettings);
		loader.load("Calibri.fnt", BitmapFont.class, bitmapSettings);

		GameIcon gameicon = new GameIcon(this, null);
		gameicon.position.set(Gdx.graphics.getWidth()*0.5f, Gdx.graphics.getHeight()*0.5f);
		
		Title title = new Title(this);
		title.setText("Loading...", Color.BLACK);
		
	}

}
