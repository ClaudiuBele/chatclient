package com.sidereal;

import java.util.ArrayList;
import java.util.List;
import com.sidereal.chatselection.ChatServer;

public class Servers 
{
	
	public static List<String> informationServers = new ArrayList<String>();
	public static List<ChatServer> chatServers = new ArrayList<ChatServer>();
	public static List<String> customServers = new ArrayList<String>();
	public static String chat;
	public static String connection;
	public static String informationServer;
	public static String nickname;
	

	/** Values used for calculating if new data should be available from the connection server
	 */
	public static long timeUntilUpdateTime, timeRequestedChatServers;

	static{
		informationServers.add("localhost:50354");
	}

}
