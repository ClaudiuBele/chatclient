package com.sidereal.login;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.dolphinoes.architecture.AbstractEvent;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.KeyTypedEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.behaviors.triggers.Hoverable;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.dolphinoes.ui.TextBuilder.Allign;
import com.sidereal.dolphinoes.ui.TextBuilder.Anchor;
import com.sidereal.dolphinoes.util.Utility;

public class InputHandler extends GameObject
{

	private Vector2 size;
	
	private Renderer renderer;
	private Clickable clickable;
	private Hoverable hoverable;
	
	private TextBuilder text, sideText;
	private boolean pressing;
	private boolean selected;
	private String internalNickname;
	
	private Color badColor;
	private Color goodColor;
	
	private float targetTooltipTransparency  = 0f;
	private float tooltipTransparency = 0f;
	
	private BlackOverlay overlay;

	
	public InputHandler(GameScene scene, BlackOverlay overlay)
	{
		
		super(scene,overlay);
	}
	
	@Override
	protected void onCreate(Object... params)
	{
		
		this.overlay = (BlackOverlay) params[0];
		
		badColor = new Color(0.8f, 0, 0, 1);
		goodColor = new Color(0, 0.8f , 0 , 1);

		internalNickname = "";
		
		this.pressing = false;
		this.size = new Vector2();
		
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		this.size.set(smallestSize* 0.5f, smallestSize*0.07f);
		
		
		NinepatchBuilder builder = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(this.size.x, this.size.y)
		.setOffsetPosition(-this.size.x/2f, -this.size.y/2f)
		.setColor(new Color(1,1,1,0.6f))
		.setScale(1, 1);

		renderer = new Renderer(this);
		renderer.addDrawer("Main", builder);
		
		
		clickable = new Clickable(this);
		// make area bigger for phone users
		clickable.setAreaSize(size.x*1.2f, size.y*1.4f , -(size.x * 1.2f )/2f, - (size.y * 1.4f)/2f);
		
		
		hoverable = new Hoverable(this);
		hoverable.setAreaSize(size.x*1.2f, size.y*1.4f , -(size.x * 1.2f )/2f, - (size.y * 1.4f)/2f);
		hoverable.setEventOnInside(new AbstractEvent()
		{
			@Override
			public void run(Object... objects)
			{
				targetTooltipTransparency = 1f;
			}
		
		});
		hoverable.setEventOnOutside(new AbstractEvent()
		{
			@Override
			public void run(Object... objects)
			{
				targetTooltipTransparency = 0f;
			}
		});
		
		ActionEvent pressEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
				pressing = true;
				return false;
			}
		};
		ActionEvent releaseEvent = new ActionEvent()
		{
			
			@Override
			public boolean run(ActionData inputData)
			{
				
				if(pressing)
				{
					selected = true;
					Gdx.input.setOnscreenKeyboardVisible(true);
					if(!Gdx.app.getType().equals(ApplicationType.Desktop))
					{
						overlay.setEnabled(true);
						overlay.setText("Nickname", internalNickname);
					}
					return false;
				}
				return false;
			}
		};
		ActionEvent backspaceEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
				if(!selected) return false;
				
				if(internalNickname.length() > 0)
				{
					internalNickname = internalNickname.substring(0, internalNickname.length()-1);
					if(internalNickname.length() == 0)
						text.setText("Enter name", badColor);
					else
						text.setText(internalNickname, goodColor);

					overlay.setText("Nickname", internalNickname);
				}
				return super.run(inputData);
			}
		};
		ActionEvent outsideEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
				if(selected)
				{
					Gdx.input.setOnscreenKeyboardVisible(false);
					overlay.setEnabled(false);
				}
				
				selected = false;
				return super.run(inputData);
			}
		};
		KeyTypedEvent keyTypedEvent = new KeyTypedEvent()
		{

			final String availableKeys = "abcdefghijklmnopqrstuvxyzwABCDEFGHIJKLMNOPQRSTUVXYZW 1234567890";
			@Override
			public boolean run(char character)
			{
			
				if(selected)
				{
					// key is a proper key
					if(availableKeys.indexOf((int)character) != -1  && internalNickname.length() < 15)
					{
						internalNickname+= character;
						overlay.text.setText(internalNickname, Color.WHITE);
						overlay.setText("Nickname", internalNickname);

						text.setText(internalNickname, goodColor);
						return false;
					}
				}
				return false;
			}
		};
		
		clickable.addActionEvent("Main", InputAction.FINGER_1, pressEvent , InputEventType.Down, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, releaseEvent, InputEventType.Up, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, outsideEvent, InputEventType.Up, false);
		DolphinOES.input.addActionEvent("Main", InputAction.KEY_BACKSPACE, backspaceEvent , InputEventType.Down);
		DolphinOES.input.addKeyTypedEvent("Main",keyTypedEvent);
		
		
		
		position.set(Gdx.graphics.getWidth()*0.5f, Gdx.graphics.getHeight()*0.825f - smallestSize * 0.07f);
		
		text = new TextBuilder(scene, false,"Calibri.fnt");
		text.setScale(smallestSize / 1800f);
		text.setText("1. Enter name", badColor);
		text.setParent(this);
		text.position.setLocalZ(1);
		
		sideText =  new TextBuilder(scene, true, "Calibri.fnt");
		sideText.setScale(smallestSize / 2200f);
		sideText.setAnchor(Anchor.Top);
		sideText.setAllign(Allign.Right);
		sideText.setParent(this);
		sideText.setWindowSize((Gdx.graphics.getWidth() -  smallestSize * 0.7f)*0.45f);
		sideText.position.setLocal(-smallestSize*0.325f, size.y/2f);
		sideText.addText("1. Enter a nickname which will be displayed along with your messages in chat rooms. The name does not have to be unique.", Color.BLACK);
		sideText.addText("", Color.BLACK);
		sideText.addText("Allowed characters in the name are letters, numbers and spaces", Color.BLACK);
	}
	
	@Override
	protected void onUpdate()
	{
		tooltipTransparency = Utility.lerpTowards(
			tooltipTransparency, 
			targetTooltipTransparency,
			4 * DolphinOES.time.getDeltaTime() );
		
		sideText.setAlpha(tooltipTransparency);
	}
	
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = x > y ? y : x ;
		this.size.set(smallestSize* 0.5f, smallestSize*0.07f);

		renderer.getDrawer("Main", NinepatchDrawer.class).setSize(size.x, size.y);
		renderer.getDrawer("Main", NinepatchDrawer.class).setOffsetPosition(-size.x/2f, -size.y/2f);
		
		clickable.setAreaSize(size.x, size.y, -size.x/2f, -size.y/2f);
		text.setScale(smallestSize / 1800f);

		position.set(Gdx.graphics.getWidth()*0.5f, Gdx.graphics.getHeight()*0.825f - smallestSize * 0.07f );

		sideText.position.setLocal(-smallestSize*0.325f, size.y/2f);
		sideText.setWindowSize((Gdx.graphics.getWidth() -  smallestSize * 0.7f)*0.45f);
		sideText.setScale(smallestSize / 2200f);

		hoverable.setAreaSize(size.x*1.2f, size.y*1.4f , -(size.x * 1.2f )/2f, - (size.y * 1.4f)/2f);

		
		
	}
	
	public boolean isValid()
	{
		return internalNickname.length() != 0;
	}
	
	public String getNickname()
	{
		return internalNickname;
	}

}
