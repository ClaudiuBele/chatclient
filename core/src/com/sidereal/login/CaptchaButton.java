package com.sidereal.login;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;

public class CaptchaButton extends GameObject
{
	private Vector2 size;
	
	private Renderer renderer;
	private Clickable clickable;
	private Integer value;
	private PinHandler pin;
	private TextBuilder text;
	
	
	public CaptchaButton(GameScene scene, Integer value, PinHandler output)
	{
		super(scene, value, output);
	}
	
	@Override
	protected void onCreate(Object... params)
	{
		
		
		
		this.value = (Integer) params[0];
		this.pin = (PinHandler) params[1];
		
		
		super.onCreate(params);
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		size = new Vector2(smallestSize/12f, smallestSize/12f);
		
		
		NinepatchBuilder  builder = new NinepatchBuilder("button_9.png", 6, 6, 5, 11)
			.setSize(size.x, size.y)
			.setOffsetPosition(-size.x/2f, -size.y/2f)
			.setScale(2, 2);
		
		renderer = new Renderer(this);
		renderer.addDrawer("Main", builder);
		
		clickable = new Clickable(this);
		clickable.setAreaSize(size.x, size.y, -size.x/2f, -size.y/2f);
		clickable.addActionEvent("Main", InputAction.FINGER_1, new ActionEvent()
		{
			
			@Override
			public boolean run(ActionData inputData)
			{
				if(value >= 0)
					pin.internalPin  += value.toString();
				else if(value == -2 )
					pin.internalPin = "";
				else if(value == -1  && pin.internalPin.length() > 0)
					pin.internalPin = pin.internalPin.substring(0, pin.internalPin.length()-1);
					
				pin.updatePin();
				return false;
			}
		}, InputEventType.Up, true);
		
		text = new TextBuilder(scene, true, "Calibri.fnt");
		if(value >= 0)
			text.setText(value+"", Color.BLACK);
		else if(value == -1)
			text.setText("<", Color.ORANGE);
		else
			text.setText("X", Color.RED);
		text.setParent(this);
		text.position.setLocalZ(1);
		text.setScale(smallestSize / 1500f);
		text.position.setLocalY(size.y*0.05f);
		if(value == 0) position.set(Gdx.graphics.getWidth()*0.5f, Gdx.graphics.getHeight()*0.5f - 3 * size.y);
		else if(value == -1) position.set(Gdx.graphics.getWidth()*0.5f - size.x, Gdx.graphics.getHeight()*0.5f - 3 * size.y);
		else if(value == -2) position.set(Gdx.graphics.getWidth()*0.5f + size.x, Gdx.graphics.getHeight()*0.5f - 3 * size.y);
		else position.set( ((value - 1)%3) * size.x + Gdx.graphics.getWidth()*0.5f - size.x, - ((value-1)/3) * size.y + Gdx.graphics.getHeight()*0.5f );

	}
	
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		size = new Vector2(smallestSize/12f, smallestSize/12f);
		renderer.getDrawer("Main", NinepatchDrawer.class).setSize(size.x, size.y);
		renderer.getDrawer("Main", NinepatchDrawer.class).setOffsetPosition(-size.x/2f, -size.y/2f);
		clickable.setAreaSize(size.x, size.y, -size.x/2f, -size.y/2f);
		text.setScale(smallestSize / 1500f);
		text.position.setLocalY(size.y*0.05f);

		if(value == 0) position.set(Gdx.graphics.getWidth()*0.5f, Gdx.graphics.getHeight()*0.5f - 3 * size.y);
		else if(value == -1) position.set(Gdx.graphics.getWidth()*0.5f - size.x, Gdx.graphics.getHeight()*0.5f - 3 * size.y);
		else if(value == -2) position.set(Gdx.graphics.getWidth()*0.5f + size.x, Gdx.graphics.getHeight()*0.5f - 3 * size.y);
		else position.set( ((value - 1)%3) * size.x + Gdx.graphics.getWidth()*0.5f - size.x, - ((value-1)/3) * size.y + Gdx.graphics.getHeight()*0.5f );
	}

}
