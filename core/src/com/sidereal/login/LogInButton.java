package com.sidereal.login;

import java.io.UnsupportedEncodingException;
import java.util.Comparator;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.Servers;
import com.sidereal.chatselection.ChatSelectionScreen;
import com.sidereal.chatselection.ChatServer;
import com.sidereal.dolphinoes.architecture.AbstractEvent;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.util.LoadingSystem;
import com.sidereal.util.Notification;
import com.sidereal.utility.Encryption;

public class LogInButton extends GameObject
{

	private Vector2 size;

	private Renderer renderer;
	private Clickable clickable;
	private TextBuilder text;
	private boolean pressing;

	private PinHandler pin;
	private InputHandler input;

	public LogInButton(GameScene scene, PinHandler pin, InputHandler input)
	{

		super(scene, pin, input);

	}

	@Override
	protected void onCreate(Object... params)
	{

		// get data from constructor
		this.pin = (PinHandler) params[0];
		this.input = (InputHandler) params[1];

		// initialize default values.
		this.pressing = false;
		this.size = new Vector2();
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();
		this.size.set(smallestSize * 0.5f, smallestSize * 0.07f);

		NinepatchBuilder builder = new NinepatchBuilder("button_9.png", 6, 6, 5, 11).setSize(this.size.x, this.size.y)
			.setOffsetPosition(-this.size.x / 2f, -this.size.y / 2f).setColor(Color.WHITE).setScale(2, 2);

		renderer = new Renderer(this);
		renderer.addDrawer("Main", builder);

		clickable = new Clickable(this);
		clickable.setAreaSize(size.x * 1.2f, size.y * 1.4f, -(size.x * 1.2f) / 2f, -(size.y * 1.4f) / 2f);

		ActionEvent pressEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{

				pressing = true;
				return true;
			}
		};
		ActionEvent releaseEvent = new ActionEvent()
		{

			@Override
			public boolean run(ActionData inputData)
			{


				if (Notification.instance.isDisplayingMessage() || LoadingSystem.instance.displayed())
					return false;

				if (pressing)
				{

					if (!input.isValid())
					{
						Notification.instance.add(
							"Entered nickname is not valid. \r\nPlease enter a nickname by pressing the "
								+ "input box and writing the desired name", Color.WHITE, true);
						return true;
					}

					// set nickname
					Servers.nickname = input.getNickname();

					if (!pin.isValid())
					{
						Notification.instance.add(
							"Entered pin code does not match the one required by the system. \r\n"
								+ "Fix the error and try again", Color.WHITE, true);
						return true;
					}

					getConnectionServers(0);

					return true;
				}
				return false;
			}
		};

		clickable.addActionEvent("Main", InputAction.FINGER_1, pressEvent, InputEventType.Down, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, releaseEvent, InputEventType.Up, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{

				pressing = false;
				return false;
			}
		}, InputEventType.Down, false);
		position.set(Gdx.graphics.getWidth() * 0.5f, Gdx.graphics.getHeight() * 0.85f);

		text = new TextBuilder(scene, true, "Blocks.fnt");
		text.setScale(smallestSize / 700f);
		text.setText("Log In", Color.BLACK);
		text.setParent(this);
		text.position.setLocalZ(1);

	}

	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{

		float smallestSize = x > y ? y : x;
		this.size.set(smallestSize * 0.5f, smallestSize * 0.07f);

		renderer.getDrawer("Main", NinepatchDrawer.class).setSize(size.x, size.y);
		renderer.getDrawer("Main", NinepatchDrawer.class).setOffsetPosition(-size.x / 2f, -size.y / 2f);

		clickable.setAreaSize(size.x, size.y, -size.x / 2f, -size.y / 2f);
		clickable.setAreaSize(size.x * 1.2f, size.y * 1.4f, -(size.x * 1.2f) / 2f, -(size.y * 1.4f) / 2f);

		text.setScale(smallestSize / 700f);

		position.set(Gdx.graphics.getWidth() * 0.5f, Gdx.graphics.getHeight() * 0.85f);

	}

	public static void getConnectionServers(final int index)
	{

		if (index > Servers.informationServers.size() - 1)
		{
			LoadingSystem.instance.timer.setEvent(0.25f, new AbstractEvent()
			{
				@Override
				public void run(Object... objects)
				{

					Notification.instance.add(
						"Unable to contact server. Server may be offline or you don't have a stable "
							+ "internet connection.\r\n\r\nPlease try again.", Color.WHITE, true);
				}
			});
			LoadingSystem.instance.display(false);

			return;
		}

		System.out.println("Tring to connect to "+Servers.informationServers.get(index));
		HttpRequest getConnectionServer = new HttpRequest(HttpMethods.GET);
		LoadingSystem.instance.display(true);
		getConnectionServer.setUrl("http://" + Servers.informationServers.get(index) + "/connectionServer");
		getConnectionServer.setTimeOut(1000);
		Gdx.net.sendHttpRequest(getConnectionServer, new HttpResponseListener()
		{

			@Override
			public void handleHttpResponse(HttpResponse httpResponse)
			{

				try
				{
					// get connection server from load balancer on information server.
					String connectionServer = new String(Encryption.decrypt(httpResponse.getResult()), "UTF-8");

					// no data from information server regarding connection server.
					if(connectionServer.length() == 0)
					{
						getConnectionServers(index + 1);
						return;
					}
					
					// server is local ( in relation to the information server, try to change it to
					// the ip we used to contact the IS with
					if (connectionServer.contains("127.0.0.1") || !connectionServer.contains(":50355"))
					{
						connectionServer = Servers.informationServers.get(index).split(":")[0] + ":50355";
					}

					System.out.println("Response for /connectionServer: " + connectionServer+ " "+connectionServer.length());

					// get chat servers tied to the connection server
					getChatServers(connectionServer, Servers.informationServers.get(index));

				} catch (UnsupportedEncodingException e)
				{
					getConnectionServers(index + 1);
				}

			}

			@Override
			public void failed(Throwable t)
			{

				getConnectionServers(index + 1);
			}

			@Override
			public void cancelled()
			{

			}
		});

	}

	public static boolean getChatServers(final String connectionServer, final String informationServer)
	{

		// make request to connection server for chat Servers
		HttpRequest getChatServers = new HttpRequest(HttpMethods.GET);
		getChatServers.setTimeOut(1000);
		getChatServers.setUrl("http://" + connectionServer + "/chatServers");
		Gdx.net.sendHttpRequest(getChatServers, new HttpResponseListener()
		{

			@Override
			public void handleHttpResponse(HttpResponse httpResponse)
			{

				// got the chat servers!
				try
				{
					// get data for all of the chat servers on the connection servers
					String decodedData = new String(Encryption.decrypt(httpResponse.getResult()), "UTF-8");

					
					String[] individualData = decodedData.split("\\{M\\}");
					
					// will be used in the refresh functionality, to detect if we actually need to refresh
					Servers.timeUntilUpdateTime =  Long.parseLong(individualData[0]);
					Servers.timeRequestedChatServers = System.currentTimeMillis();
					
					// we got this far, means connect server is working properly.
					Servers.connection = connectionServer;

					// split data from connServer/chatServers
					String[] chatServers;

					System.out.println(decodedData);
					
					Servers.chatServers.clear();

					// no data can be found
					if(individualData.length == 1 || !individualData[1].contains("{V}"))
					{
						LoadingSystem.instance.runOnGameThread(new AbstractEvent()
						{
							@Override
							public void run(Object... objects) {
								
								// done, disabling the Loading and changing the scene
								LoadingSystem.instance.display(false);
								Servers.informationServer = informationServer;
								DolphinOES.setScene(new ChatSelectionScreen());
							};
						});
						return;
					}
					
					
					if (decodedData.contains("{O}"))
						chatServers = individualData[1].split("\\{O\\}");
					else
						chatServers = new String[] {individualData[1]};

					for (int i = 0; i < chatServers.length; i++)
					{
						// parse data from the connection server
						String[] chatServerData = chatServers[i].split("\\{V\\}");

						// we connected to an IS with an address that might not be localhost, and it returned us some
						// connection servers on localhost ( at the same computer ), so we use the same
						// address we connected with and the designated chat server port
						if (chatServerData[1].contains("127.0.0.1") && Servers.customServers.size() > 0)
						{
							ChatServer server = new ChatServer(chatServerData[0], informationServer.split(":")[0]
								+ ":50356", chatServerData[2], Long.parseLong(chatServerData[3]), Long
								.parseLong(chatServerData[4]));

							System.out.println("Added server with address: " + server.serverAddress);
							Servers.chatServers.add(server);

						} 
						// chat server is not on same address as server
						else
						{
							ChatServer server = new ChatServer(chatServerData[0], chatServerData[1], chatServerData[2],
								Long.parseLong(chatServerData[3]), Long.parseLong(chatServerData[4]));
							System.out.println("Added server with address: " + server.serverAddress);
							Servers.chatServers.add(server);

						}

					}

					System.out.println("Sorting stuff");
					// sort chat servers
					java.util.Collections.sort(Servers.chatServers, new Comparator<ChatServer>()
					{
						@Override
						public int compare(ChatServer o1, ChatServer o2)
						{
							return (int) (o1.dbCallsPerMinute * 10 + o1.requestsPerMinute - (o2.dbCallsPerMinute * 10 + o2.requestsPerMinute));
						}
					});
					
					// go to new scene
					LoadingSystem.instance.runOnGameThread(new AbstractEvent()
					{
						public void run(Object... objects) {
							
							// done, disabling the Loading and changing the scene
							LoadingSystem.instance.display(false);
							Servers.informationServer = informationServer;
							DolphinOES.setScene(new ChatSelectionScreen());
							
						};
					});
					

				} catch (Exception e)
				{
					System.out.println(e.getMessage());
					getConnectionServers(Servers.informationServers.indexOf(informationServer) + 1);
//					e.printStackTrace();
				}

			}

			@Override
			public void failed(Throwable t)
			{
				// if this is the last one to fail, show error
//				if(Servers.informationServers.get(Servers.informationServers.size()-1).equals(informationServer))
//				{
					getConnectionServers(Servers.informationServers.indexOf(informationServer) + 1);
//				}
			}

			@Override
			public void cancelled()
			{

			}
		});

		return false;
	}
}
