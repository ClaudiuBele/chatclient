package com.sidereal.login;

import java.lang.ref.WeakReference;
import java.util.Random;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.AbstractEvent;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.behaviors.triggers.Hoverable;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.dolphinoes.ui.TextBuilder.Allign;
import com.sidereal.dolphinoes.ui.TextBuilder.Anchor;
import com.sidereal.dolphinoes.util.Utility;

public class PinHandler extends GameObject
{
	public String pin;
	public String internalPin;
	private TextBuilder text, sideText;
	
	private String captcha;
	
	private Color badColor;
	private Color goodColor;
	
	private float targetTooltipTransparency  = 0f;
	private float tooltipTransparency = 0f;
	
	private Hoverable hoverable;

	
	public PinHandler(GameScene scene)
	{
		super(scene);
		
	}

	@Override
	protected void onCreate(Object... params)
	{
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();

		
		badColor = new Color(0.8f, 0, 0, 1);
		goodColor = new Color(0, 0.8f, 0, 1);
		
		WeakReference<Random> ref = new WeakReference<Random>(new Random());
		captcha ="";
		for(int i=0;i<6 ;i++)
		{
			captcha+= ref.get().nextInt(10);
		}
		
		internalPin = "";
		pin= "2. Enter code above using buttons";
		
		position.set(Gdx.graphics.getWidth()*0.5f, Gdx.graphics.getHeight()*0.675f);

		
		hoverable = new Hoverable(this);
		hoverable.setAreaSize(smallestSize*0.4f, smallestSize*0.52f, -smallestSize*0.2f, -smallestSize*0.5f);
		hoverable.setEventOnInside(new AbstractEvent()
		{
			@Override
			public void run(Object... objects)
			{
				targetTooltipTransparency = 1f;
			}
		
		});
		hoverable.setEventOnOutside(new AbstractEvent()
		{
			@Override
			public void run(Object... objects)
			{
				targetTooltipTransparency = 0f;
			}
		});
		
		text = new TextBuilder(scene, true, "Calibri.fnt");
		text.setAnchor(Anchor.Top);
		text.setParent(this);
		text.position.setLocal(0, 0);
		updatePin();
		
		text.setScale(smallestSize/1800f);
		text.setWindowSize(smallestSize*0.5f);
		
		sideText = new TextBuilder(scene, true, "Calibri.fnt");
		sideText.addText("2. Enter the generated pin using the buttons below", Color.BLACK);
		sideText.addText("", Color.BLACK);
		sideText.addText("This is done in order to avoid people flooding the system and overloading the servers", Color.BLACK);
		sideText.setScale(smallestSize/2200f);
		sideText.setParent(this);
		sideText.setAllign(Allign.Left);
		sideText.setAnchor(Anchor.Top);
		sideText.position.setLocal(smallestSize*0.325f, 0);
		sideText.setWindowSize((Gdx.graphics.getWidth() -  smallestSize * 0.7f)*0.45f);
		
	}	
	
	@Override
	protected void onUpdate()
	{
		tooltipTransparency = Utility.lerpTowards(
			tooltipTransparency, 
			targetTooltipTransparency,
			4 * DolphinOES.time.getDeltaTime() );
		
		sideText.setAlpha(tooltipTransparency);
	}

	public void updatePin()
	{
		text.clearText();
		
		if(isValid())
			text.addText(captcha, goodColor);
		else
			text.addText(captcha, badColor);

		text.addText(((internalPin.length() == 0) ? pin : internalPin), Color.BLACK);
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
	
		position.set(Gdx.graphics.getWidth()*0.5f, Gdx.graphics.getHeight()*0.675f);
		
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		
		text.position.setLocal(0,0);
		text.setScale(smallestSize/1800f);
		text.setWindowSize(smallestSize*0.5f);

		sideText.position.setLocal(smallestSize*0.325f, 0);
		sideText.setScale(smallestSize/2200f);
		sideText.setWindowSize((Gdx.graphics.getWidth() -  smallestSize * 0.7f)*0.45f);
		
		hoverable.setAreaSize(smallestSize*0.4f, smallestSize*0.52f, -smallestSize*0.2f, -smallestSize*0.5f);


	}
	
	public boolean isValid()
	{
		return internalPin.equals(captcha);
	}
	
}
