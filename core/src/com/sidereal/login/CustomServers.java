package com.sidereal.login;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.Servers;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.KeyTypedEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;

public class CustomServers extends GameObject
{

	private Vector2 size;
	
	private Renderer renderer;
	private Clickable clickable;
	
	private TextBuilder text;
	private boolean pressing;
	private boolean selected;
	private String servers;
	
	private Color badColor;
	private Color goodColor;
	
	private BlackOverlay overlay;
	
	private ActionEvent backspaceEvent;
	private KeyTypedEvent keyTypedEvent;
	
	public CustomServers(GameScene scene, BlackOverlay overlay)
	{
		super(scene,overlay);
		
	}
	
	@Override
	protected void onCreate(Object... params)
	{
		overlay = (BlackOverlay) params[0];
		
		badColor = new Color(0.8f, 0, 0, 1);
		goodColor = new Color(0, 0.8f , 0 , 1);

		servers = "";
		
		this.pressing = false;
		this.size = new Vector2();
		
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		this.size.set(smallestSize* 0.6f, smallestSize*0.07f);
		
		
		NinepatchBuilder builder = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(this.size.x, this.size.y)
		.setOffsetPosition(-this.size.x/2f, -this.size.y/2f)
		.setColor(new Color(1,1,1,0.6f))
		.setScale(1, 1);

		renderer = new Renderer(this);
		renderer.addDrawer("Main", builder);
		
		ActionEvent pressEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{

				pressing = true;
				return false;
			}
		};
		ActionEvent releaseEvent = new ActionEvent()
		{
			
			@Override
			public boolean run(ActionData inputData)
			{

				if(pressing  && overlay.isEnabled() == false)
				{
					selected = true;
					Gdx.input.setOnscreenKeyboardVisible(true);
					if(!Gdx.app.getType().equals(ApplicationType.Desktop))
					{
						overlay.setEnabled(true);
						overlay.setText("Custom servers", servers);

					}
					return false;
				}
				return false;
			}
		};
		ActionEvent outsideEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
				

				if(selected)
				{
					Gdx.input.setOnscreenKeyboardVisible(false);
					overlay.setEnabled(false);
				}
				

				selected = false;
				
				servers.replace(" ", "");
				
				// only one value, no delimiter, check if length is appropriate
				// for a server and add the raw text itself
				if(!servers.contains(","))
				{
					if(servers.length() > 0)
					{
						Servers.informationServers.clear();
						Servers.informationServers.add("localhost:8080");
						
						if(!servers.contains(":50354")) servers+=":50354";
						Servers.informationServers.add(servers);
						Servers.customServers.add(servers);

					}
						
					return false;
				}
				
				String[] parsedServers = servers.split(",");
				
				
				
				Servers.informationServers.clear();
				Servers.informationServers.add("localhost:8080");
				for(int i=0;i< parsedServers.length;i++)
				{
					
					if(!parsedServers[i].contains(":50354")) parsedServers[i]+=":50354";
					Servers.informationServers.add(parsedServers[i]);
					Servers.customServers.add(parsedServers[i]);
				}
				
				return super.run(inputData);
			}
		};
		backspaceEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{			

				if(servers.length() > 0)
				{
					servers = servers.substring(0, servers.length()-1);
					if(servers.length() == 0)
						text.setText("Enter Custom servers, spearated by comma", badColor);
					else
						text.setText(servers, goodColor);

					overlay.setText("Custom servers", servers);

				}
				return false;
			}
		};
		keyTypedEvent = new KeyTypedEvent()
		{
			final String availableKeys = "abcdefghijklmnopqrstuvxyzwABCDEFGHIJKLMNOPQRSTUVXYZW 1234567890.:";
			@Override
			public boolean run(char character)
			{
				if(selected)
				{
					// key is a proper key
					if(availableKeys.indexOf((int)character) != -1 )
					{
						servers+= character;
						text.setText(servers, goodColor);
						overlay.setText("Custom servers", servers);

						return false;
					}
				}
				return false;
			}
		};
		
		clickable = new Clickable(this);
		// make area bigger for phone users
		clickable.setAreaSize(size.x*1.2f, size.y*1.4f , -(size.x * 1.2f )/2f, - (size.y * 1.4f)/2f);
		clickable.addActionEvent("Main", InputAction.FINGER_1, pressEvent , InputEventType.Down, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, releaseEvent, InputEventType.Up, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, outsideEvent, InputEventType.Up, false);
		DolphinOES.input.addActionEvent("Main", InputAction.KEY_BACKSPACE,backspaceEvent, InputEventType.Down);
		DolphinOES.input.addKeyTypedEvent("Main", keyTypedEvent);
		
		position.set(Gdx.graphics.getWidth()*0.5f, size.y);
		
		text = new TextBuilder(scene, true,"Calibri.fnt");
		text.setScale(smallestSize / 2200f);
		text.setText("Enter Custom servers, separated by comma", badColor);
		text.setParent(this);
		text.setWindowSize(smallestSize*0.6f);
		text.position.setLocalZ(1);
		text.setWindowSize(smallestSize*0.6f);

		
	}
	
	@Override
	protected void onUpdate()
	{
	}
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = x > y ? y : x ;
		this.size.set(smallestSize* 0.6f, smallestSize*0.07f);

		renderer.getDrawer("Main", NinepatchDrawer.class).setSize(size.x, size.y);
		renderer.getDrawer("Main", NinepatchDrawer.class).setOffsetPosition(-size.x/2f, -size.y/2f);
		
		clickable.setAreaSize(size.x, size.y, -size.x/2f, -size.y/2f);
		text.setScale(smallestSize / 3000f);
		text.setWindowSize(smallestSize*0.6f);

		position.set(Gdx.graphics.getWidth()*0.5f, size.y);

	}
	
	
	public String getServers()
	{
		return servers;
	}
	
	@Override
	protected void onDispose()
	{
		DolphinOES.input.removeKeyTypedEvent("Main", keyTypedEvent);
		DolphinOES.input.removeActionEvent("Main", backspaceEvent);

	}
	

}
