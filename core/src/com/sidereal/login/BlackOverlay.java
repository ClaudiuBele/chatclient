package com.sidereal.login;

import java.lang.ref.WeakReference;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.sprite.SpriteBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.sprite.SpriteDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.dolphinoes.ui.TextBuilder.Anchor;

public class BlackOverlay extends GameObject
{
	private Renderer renderer;
	public TextBuilder text;
	
	public static BlackOverlay instance;

	public BlackOverlay(GameScene scene, Object[] params)
	{

		super(scene, params);
		setEnabled(false);
	}
	
	@Override
	protected void onCreate(Object... params)
	{
		setName("Overlay");
		setType("UI");
		isPersistent = true;
		
		instance = this;
		
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();

		
		position.setZ(100);
		
		text = new TextBuilder(scene, true,"Calibri.fnt");
		
		text.setScale(smallestSize / 1800f);
		text.setText("\"\"", Color.WHITE);
		text.setParent(this);
		text.position.setLocalZ(1);
		text.setAnchor(Anchor.Top);
		text.setWindowSize(Gdx.graphics.getWidth()*0.9f);
		
		renderer = new Renderer(this);
		
		WeakReference<SpriteBuilder> bg = new WeakReference<SpriteBuilder>
		(new SpriteBuilder(DolphinOES.assets.frameworkAssetsFolder+"White.png").setColor(new Color(0,0,0,0.9f)));
		renderer.addDrawer("BG", bg.get());
		renderer.placeAtStart("BG");
		
		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);
		
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = x > y ? y : x ;

		renderer.getDrawer("BG",SpriteDrawer.class)
		.setSizeAndCenter(x, y);

		position.set(x/2f, y/2f, 10);
		
		text.setScale(smallestSize / 1800f);
		text.position.set(x/2f, y*0.9f);
		text.setWindowSize(x*0.9f);
		
	}
	public void setText(String title, String content)
	{
		text.clearText();
		text.addText(title+"\r\n", Color.LIGHT_GRAY);
		text.addText("\r\n"+content, Color.WHITE);
		text.addText("\r\nPress anywhere to close the overlay", Color.LIGHT_GRAY);

	}

}
