package com.sidereal.login;

import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.splash.Title;
import com.sidereal.util.LoadingSystem;
import com.sidereal.util.Notification;

public class LoginScreen extends GameScene
{
	@SuppressWarnings("unused")
	@Override
	public void createScene()
	{
		new LoadingSystem(this, null);
		new Notification(this);
		

		BlackOverlay overlay = new BlackOverlay(this, null);

		final InputHandler input = new InputHandler(this, overlay);
		new CustomServers(this, overlay);

		final PinHandler pin = new PinHandler(this);

		LogInButton button = new LogInButton(this, pin, input);

		for (int i = -2; i < 10; i++)
		{
			new CaptchaButton(this, i, pin);
		}

		Background background = new Background(this, input, pin);

		((Title) get("UI", "Title")).setText("Authentication", Color.BLACK);
	}

}
