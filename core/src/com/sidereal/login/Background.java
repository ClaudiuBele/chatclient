package com.sidereal.login;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;

public class Background extends GameObject
{
	@SuppressWarnings("unused")
	private InputHandler input;
	@SuppressWarnings("unused")
	private PinHandler pin;
	
	public Background(GameScene scene, Object... params)
	{
		super(scene);
		input = (InputHandler) params[0];
		pin = (PinHandler) params[1];
	}


	private Renderer renderer;
	private Color internalLoginColor;
	
	@Override
	protected void onCreate(Object... params)
	{
	
		super.onCreate(params);
		
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();

		
//		internalLoginColor = new Color(1,1,1,0.15f);
		internalLoginColor = new Color(1,1,1,1);

		NinepatchBuilder ninepatch = new NinepatchBuilder("background_9.png", 7, 7, 7, 7)
			.setSize(smallestSize*0.6f, smallestSize*0.8f)
			.setOffsetPosition(-smallestSize*0.3f, -smallestSize*0.4f)
			.setColor(internalLoginColor)
			.setScale(2, 2);
		
		position.set(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f,-1);
		
		renderer = new Renderer(this);
		renderer.addDrawer("Main", ninepatch);
		
		
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();
		position.set(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f);
		renderer.getDrawer("Main", NinepatchDrawer.class).setSize(smallestSize*0.6f, smallestSize*0.8f);
		renderer.getDrawer("Main", NinepatchDrawer.class).setOffsetPosition(-smallestSize*0.3f, -smallestSize*0.4f);


	}
}
