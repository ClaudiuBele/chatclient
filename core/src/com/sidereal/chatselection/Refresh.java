package com.sidereal.chatselection;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.Servers;
import com.sidereal.dolphinoes.architecture.AbstractEvent;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.behaviors.renderer.sprite.SpriteBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.sprite.SpriteDrawer;
import com.sidereal.login.LogInButton;
import com.sidereal.util.LoadingSystem;

public class Refresh extends GameObject
{

	private Renderer renderer;
	private Clickable clickable;
	private float angle;
	
	public Refresh(GameScene scene, Object[] params)
	{

		super(scene, params);
	}
	
	@Override
	protected void onCreate(Object... params)
	{
	
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();
		
		renderer = new Renderer(this);
		
		SpriteBuilder texture = new SpriteBuilder("refresh.png")
		.setSize(new Vector2(smallestSize*0.1f, smallestSize*0.1f))
		.setOffsetPosition(new Vector2(0, 0));
		
		NinepatchBuilder ninepatch = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(smallestSize*0.1f, smallestSize*0.1f)
		.setOffsetPosition(0, 0);
		
		renderer.addDrawer("bg", ninepatch);
		renderer.addDrawer("main", texture);
		
		clickable = new Clickable(this);
		clickable.setAreaSize(smallestSize*0.1f, smallestSize*0.1f, 0, 0);
		clickable.addActionEvent("Main", InputAction.FINGER_1, new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
				LoadingSystem.instance.display(true);
				if(System.currentTimeMillis() - Servers.timeRequestedChatServers > Servers.timeUntilUpdateTime)
				{
					LogInButton.getChatServers(Servers.connection, Servers.informationServer);
				}
				else
				{
					LoadingSystem.instance.display(true);
					LoadingSystem.instance.timer.setEvent(0.4f, new AbstractEvent()
					{
						@Override
						public void run(Object... objects)
						{

							LoadingSystem.instance.display(false);
						}
					});
				}
				// TODO Auto-generated method stub
				return super.run(inputData);
			}
			
		}, InputEventType.Up, true);
		
		
		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);
	}
	
	@Override
	protected void onUpdate()
	{
		angle+= DolphinOES.time.getDeltaTime() * 360f;
		renderer.getDrawer("main" , SpriteDrawer.class).setRotation(angle%360, false);
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		
		float smallestSize = x > y ? y : x;

		
		renderer.getDrawer("bg", NinepatchDrawer.class).setSize(smallestSize*0.1f, smallestSize*0.1f);
		renderer.getDrawer("main", SpriteDrawer.class).setSize(smallestSize*0.1f, smallestSize*0.1f);
		renderer.getDrawer("main" , SpriteDrawer.class).setOrigin(smallestSize*0.05f, smallestSize*0.05f);
		
		position.set(x-smallestSize*0.1f, y-smallestSize*0.1f);
		clickable.setAreaSize(smallestSize*0.1f, smallestSize*0.1f, 0, 0);

	}

}
