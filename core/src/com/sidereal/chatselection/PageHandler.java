package com.sidereal.chatselection;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.ui.TextBuilder;

public class PageHandler extends GameObject
{

	private PageSelector leftPage;
	private PageSelector rightPage;
	
	private TextBuilder text;
	
	private int internalPage;

	public PageHandler(GameScene scene, Object[] params)
	{
		super(scene, params);
	}

	@Override
	protected void onCreate(Object... params)
	{
		internalPage = -1;
		
		leftPage = new PageSelector(scene, true);
		leftPage.setParent(this);
		
		rightPage = new PageSelector(scene, false);
		rightPage.setParent(this);
		
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();
		
		text = new TextBuilder(scene, false, "Calibri.fnt");
		text.setScale(smallestSize/2000f);
		text.setParent(this);
		
		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);
	}
	
	@Override
	protected void onUpdate()
	{
		if(internalPage != ChatSelectionScreen.serverPage)
		{
			internalPage = ChatSelectionScreen.serverPage;
			text.setText("Page #"+(internalPage+1)+
				" (of "+(ChatSelectionScreen.servers.size()/ChatSelectionScreen.serversPerPage + 1) +" )", 
				Color.BLACK);
			
			
		}
		
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		
		float smallestSize = x > y ? y : x;

		text.setScale(smallestSize/2000f);
		position.set(Gdx.graphics.getWidth()/2f	, Gdx.graphics.getHeight()*0.1f);
	}

}
