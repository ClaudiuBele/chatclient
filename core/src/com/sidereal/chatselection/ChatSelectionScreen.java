package com.sidereal.chatselection;

import java.util.ArrayList;
import java.util.List;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.Servers;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.splash.Title;

public class ChatSelectionScreen extends GameScene
{
	public static int serverPage = 0;
	public static final int serversPerPage = 8;
	public static List<ChatServerObject> servers = new ArrayList<ChatServerObject>();
	
	
	public ChatSelectionScreen(Object... params)
	{
		super(params);
		System.out.println("Creating scene");
	}
	
	@Override
	public void createScene()
	{
		@SuppressWarnings("unused")
		PageHandler pages = new PageHandler(this, null);
		
		// create Chat objects for all chat servers retrieved from connection server
		for(int i=0 ; i < Servers.chatServers.size() ; i++)
		{
			ChatServerObject object =  new ChatServerObject(this, Servers.chatServers.get(i), i);
			if( i / serversPerPage != 0 ) object.setEnabled(false);
		}
		
		((Title) get("UI", "Title")).setText("Chat Selection", Color.BLACK);

		new Refresh(this, null);
	}

}
