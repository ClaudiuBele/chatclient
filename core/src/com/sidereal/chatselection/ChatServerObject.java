package com.sidereal.chatselection;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.Servers;
import com.sidereal.chat.ChatScreen;
import com.sidereal.dolphinoes.architecture.AbstractEvent;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.behaviors.renderer.texture.TextureBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.texture.TextureDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.dolphinoes.ui.TextBuilder.Allign;
import com.sidereal.util.LoadingSystem;
import com.sidereal.util.Notification;
import com.sidereal.utility.Encryption;

public class ChatServerObject extends GameObject
{

	private ChatServer server;
	private int index;
	private long serverLoad;

	private Renderer renderer;
	private Clickable clickable;
	private TextBuilder text;

	public ChatServerObject(GameScene scene, ChatServer server, Integer index)
	{

		super(scene, server, index);
	}

	@Override
	protected void onCreate(Object... params)
	{

		this.server = (ChatServer) params[0];
		this.index = (Integer) params[1];

		try
		{

			FileHandleResolver resolver = DolphinOES.assets.getResolver().newInstance();
			FileHandle flagFile = resolver.resolve("Flags/" + server.serverFlag+".png");
			System.out.println(flagFile.path()+" Flags/" + server.serverFlag);

			// if server has a flag that does not exist, make the flag "Unknown"
			if (!flagFile.exists())
				server.serverFlag = "Unknown";

			float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
				: Gdx.graphics.getWidth();

			// region clickable

			clickable = new Clickable(this);
			clickable.setAreaSize(smallestSize * 0.8f, smallestSize * 0.1f);
			// add event for contacting chat server and signing up for it
			clickable.addActionEvent("Main", InputAction.FINGER_1, new ActionEvent()
			{

				public boolean run(ActionData inputData)
				{
					if(serverLoad > 2000 )
					{
						Notification.instance
						.add(
							"Chat server is under heavy load. Choose another server or try again.",
							Color.WHITE, true);
						
						return true;
					}

					LoadingSystem.instance.display(true);

					HttpRequest enterChatServer = new HttpRequest(HttpMethods.POST);
					// Url for contacting chat server
					enterChatServer.setUrl("http://" + server.serverAddress + "/client");
					// encrypt nickname and put it as the contetnt of the POST request

					byte[] bytes = Encryption.encrypt(Servers.nickname.getBytes());
					ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
					enterChatServer.setHeader("Content-Type", "text/plain");
					enterChatServer.setContent(byteStream, bytes.length);


					Gdx.net.sendHttpRequest(enterChatServer, new HttpResponseListener()
					{

						@Override
						public void handleHttpResponse(HttpResponse httpResponse)
						{

							// get extracted data and decrypt it.
							try
							{

								String response = new String(Encryption.decrypt(httpResponse.getResult()), "UTF-8");
								
								System.out.println("Response from chat server: "+response);
								
								if (response.equals("true"))
								{
									// server got us chat
									Servers.chat = server.serverAddress;
									// we got accepted to the chat server

									// go to next scene
									LoadingSystem.instance.timer.setEvent(0.5f, new AbstractEvent()
									{
										@SuppressWarnings("static-access")
										@Override
										public void run(Object... objects)
										{

											DolphinOES.getInstance().setScene(new ChatScreen());
										}
									});
									// disable the fake loading
									LoadingSystem.instance.display(false);

								} else
								{
									Notification.instance
										.add(
											"Unable to connect to the chat servers.\r\nTry again, if the problem persists, connect to another server",
											Color.WHITE, true);
									LoadingSystem.instance.display(false);
								}

							} catch (UnsupportedEncodingException e)
							{
								Notification.instance
									.add(
										"An error occured while retrieving data from chat server.\r\nTry again, if the problem persists, connect to another server",
										Color.WHITE, true);
								LoadingSystem.instance.display(false);
							}
						}

						@Override
						public void failed(Throwable t)
						{

							Notification.instance
								.add(
									"Unable to connect to the chat servers.\r\nTry again, if the problem persists, connect to another server",
									Color.WHITE, true);
							LoadingSystem.instance.display(false);

						}

						@Override
						public void cancelled()
						{

						}
					});

					return true;
				};

			}, InputEventType.Up, true);

			// endregion clickable
			// region renderer

			renderer = new Renderer(this);

			// make load balancer data
			NinepatchBuilder ninepatch = new NinepatchBuilder("background_9.png", 7, 7, 7, 7)
				.setColor(Color.LIGHT_GRAY);

			NinepatchBuilder bg = new NinepatchBuilder("background_9.png", 7, 7, 7, 7).setSize(smallestSize * 0.8f,
				smallestSize * 0.1f).setOffsetPosition(-smallestSize * 0.4f, -smallestSize * 0.05f);

			TextureBuilder flag = new TextureBuilder("Flags/" + server.serverFlag + ".png").setSize(
				smallestSize * 0.06f, smallestSize * 0.06f).setOffsetPosition(
				-smallestSize * 0.03f + smallestSize * 0.3f, -smallestSize * 0.03f);

			for (int i = 0; i < 4; i++)
			{
				if (i == 3)
					// signal GC to collect builder as we're done with it.
					renderer.addDrawer("cell" + i, ninepatch, true);
				else
					renderer.addDrawer("cell" + i, ninepatch, false);
			}
			renderer.addDrawer("flag", flag);
			renderer.addDrawer("bg", bg);
			// renderer.addDrawer("flag", sprite);
			renderer.placeAtStart("bg"); // render background first
			// renderer.getDrawer("bg",NinepatchDrawer.class).setColor(new Color(1,1,1,0.3f));

			// endregion renderer
			
			text = new TextBuilder(scene, false, "Calibri.fnt");
			text.setAllign(Allign.Left);
			text.setParent(this);
			text.setText(server.serverName, Color.BLACK);
			text.position.setLocalZ(1);
			
			// force resize to not have to write twice adaptive code
			onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);
			
			updateLoad();

		} catch (InstantiationException e)
		{
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}

		super.onCreate(params);
	}

	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{

		float smallestSize = x > y ? y : x;

		renderer.getDrawer("flag", TextureDrawer.class)
			.setSize(new Vector2(smallestSize * 0.08f, smallestSize * 0.08f));
		renderer.getDrawer("flag", TextureDrawer.class).setOffsetPosition(
			new Vector2(smallestSize * 0.4f - smallestSize * 0.08f * 1.2f, -smallestSize * 0.04f));

		for (int i = 0; i < 4; i++)
		{
			// default size is in width: 3% of the smallest dimension ( width or height )
			// height : 8% of smallest dimension multiplied by how much the flag height's is in the flag image ( 70% )
			renderer.getDrawer("cell" + i, NinepatchDrawer.class).setSize(smallestSize * 0.02f,
				smallestSize * 0.08f * 0.7f);
			renderer.getDrawer("cell" + i, NinepatchDrawer.class).setOffsetPosition(
			// displace them by 5% of smallest dimension between each cell
			// as well as all of them by 15% from the center of the screen
			// and center around their own size by decreasing half the size
				-smallestSize * 0.01f + smallestSize * 0.21f + smallestSize * i * 0.025f, -smallestSize * 0.04f * 0.7f);
		}
		// resize background
		renderer.getDrawer("bg", NinepatchDrawer.class).setSize(smallestSize * 0.8f, smallestSize * 0.08f);
		renderer.getDrawer("bg", NinepatchDrawer.class).setOffsetPosition(-smallestSize * 0.4f, -smallestSize * 0.04f);

		position.set(x / 2f, y * 0.85f - (index % ChatSelectionScreen.serversPerPage ) * 0.1f * y);

		clickable.setAreaSize(smallestSize * 0.8f, smallestSize * 0.08f);
		
		text.position.setLocal(-smallestSize*0.38f, 0);
		text.setScale(smallestSize/3000f);
		
		super.onResize(x, y, oldX, oldY);
	}

	protected void updateLoad()
	{
		serverLoad = server.dbCallsPerMinute * 10 + server.requestsPerMinute;
		
		if(serverLoad < 100)
			renderer.getDrawer("cell0", NinepatchDrawer.class).setColor(Color.GREEN);
		else if(serverLoad < 500)
		{
			renderer.getDrawer("cell0", NinepatchDrawer.class).setColor(Color.YELLOW);
			renderer.getDrawer("cell1", NinepatchDrawer.class).setColor(Color.YELLOW);
		}
		else if(serverLoad < 2000)
		{
			renderer.getDrawer("cell0", NinepatchDrawer.class).setColor(Color.ORANGE);
			renderer.getDrawer("cell1", NinepatchDrawer.class).setColor(Color.ORANGE);
			renderer.getDrawer("cell2", NinepatchDrawer.class).setColor(Color.ORANGE);
		}
		else
		{
			renderer.getDrawer("cell0", NinepatchDrawer.class).setColor(Color.RED);
			renderer.getDrawer("cell1", NinepatchDrawer.class).setColor(Color.RED);
			renderer.getDrawer("cell2", NinepatchDrawer.class).setColor(Color.RED);
			renderer.getDrawer("cell3", NinepatchDrawer.class).setColor(Color.RED);
		}
	}
	
}
