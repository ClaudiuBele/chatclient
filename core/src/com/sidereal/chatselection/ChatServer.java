package com.sidereal.chatselection;

public class ChatServer
{
	public final String serverName;
	public final String serverAddress;
	public String serverFlag;
	public final long requestsPerMinute;
	public final long dbCallsPerMinute;
	
	public ChatServer(String name, String address, String flag, long requestsPerMinute, long dbCallsPerMinute)
	{
		this.serverName = name;
		this.serverAddress = address;
		this.serverFlag = flag;
		this.requestsPerMinute = requestsPerMinute;
		this.dbCallsPerMinute = dbCallsPerMinute;
	}
}
