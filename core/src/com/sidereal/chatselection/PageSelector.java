package com.sidereal.chatselection;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.sprite.SpriteBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.sprite.SpriteDrawer;

public class PageSelector extends GameObject
{

	private boolean left;
	private Renderer renderer;
	private Clickable clickable;
	
	public PageSelector(GameScene scene, Boolean left)
	{
		super(scene, left);
	}

	@Override
	protected void onCreate(Object... params)
	{
		left = (Boolean)params[0];
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();
		
		renderer = new Renderer(this);
		
		SpriteBuilder sprite = new SpriteBuilder("grey_slider.png")
		.setSize(new Vector2(smallestSize*0.07f, smallestSize*0.04f))
		.setOffsetPosition(new Vector2(-smallestSize*0.035f, -smallestSize * 0.02f));
		
		renderer.addDrawer("main", sprite);
		
		clickable = new Clickable(this);
		clickable.setAreaSize(smallestSize*0.07f, smallestSize*0.04f);
		
		clickable.addActionEvent("Main", InputAction.FINGER_1, new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
			
				boolean canDo = true;
				if(ChatSelectionScreen.serverPage == 0 ||
					ChatSelectionScreen.serverPage == (ChatSelectionScreen.servers.size() - 1 ) / ChatSelectionScreen.serversPerPage)
					canDo = false;
				
				if(canDo)
				{
					for(int i=ChatSelectionScreen.serverPage * ChatSelectionScreen.serversPerPage ;
						i < (ChatSelectionScreen.serverPage + 1) * ChatSelectionScreen.serversPerPage ;
						i++)
					{
						if(i < ChatSelectionScreen.servers.size()-1)
							ChatSelectionScreen.servers.get(i).setEnabled(false);
					}
					
					// increment or decrement server page
					ChatSelectionScreen.serverPage = ChatSelectionScreen.serverPage +
						( left  ? -1 : 1 );
					
					for(int i=ChatSelectionScreen.serverPage * ChatSelectionScreen.serversPerPage ;
						i < (ChatSelectionScreen.serverPage + 1) * ChatSelectionScreen.serversPerPage ;
						i++)
					{
						if(i < ChatSelectionScreen.servers.size()-1)
							ChatSelectionScreen.servers.get(i).setEnabled(true);
					}
					
				}
				// TODO Auto-generated method stub
				return super.run(inputData);
			}
		}, 
		InputEventType.Up,
		true);
		
		
		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = x > y ? y : x;

		if(left) position.setLocal(-smallestSize*0.3f, 0f);
		else position.setLocal(smallestSize*0.3f, 0f);
		
		
	
		renderer.getDrawer("main", SpriteDrawer.class).setSize(smallestSize*0.07f, smallestSize*0.04f);
		renderer.getDrawer("main", SpriteDrawer.class).setOffsetPosition(-smallestSize*0.035f, -smallestSize * 0.02f);
	
		if(!left)
		{
			renderer.getDrawer("main",SpriteDrawer.class).setOrigin(smallestSize*0.035f, smallestSize * 0.02f);
			renderer.getDrawer("main",SpriteDrawer.class).setRotation(180, true);
		}
		
		clickable.setAreaSize(smallestSize*0.07f, smallestSize*0.04f);
	}
	
}
