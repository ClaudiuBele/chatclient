package com.sidereal.chat;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.Net.Protocol;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.net.ServerSocket;
import com.badlogic.gdx.net.ServerSocketHints;
import com.badlogic.gdx.net.Socket;
import com.sidereal.Servers;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.splash.Title;
import com.sidereal.utility.Encryption;

public class ChatScreen extends GameScene
{

	public static List<Chatroom> chatrooms = new ArrayList<Chatroom>();
	public static Chatroom currChatroom;
	
	private ServerSocket server; 
	
	@Override
	public void createScene()
	{
		((Title) get("UI", "Title")).setText("5 MIN", Color.BLACK);

		@SuppressWarnings("unused")
		ChatroomHandler join = new ChatroomHandler(this);
		
		new ChatFrame(this, null);
		
		// Now we create a thread that will listen for incoming socket connections
        new Thread(new Runnable(){

            @Override
            public void run() {
                ServerSocketHints serverSocketHint = new ServerSocketHints();
                // 0 means no timeout.  Probably not the greatest idea in production!
                serverSocketHint.acceptTimeout = 0;
                
                // Create the socket server using TCP protocol and listening on port 50357
                // Only one app can listen to a port at a time, keep in mind many ports are reserved
                // especially in the lower numbers ( like 21, 80, etc )
                server = Gdx.net.newServerSocket(Protocol.TCP, 50357, serverSocketHint);
                
                // Loop forever
                while(true){
                	
                	try {
                		
	                    // Create a socket
	                    Socket socket = server.accept(null);
	                    
	                    // Read data from the socket into a BufferedReader
	                    DataInputStream buffer = new DataInputStream(socket.getInputStream()); 
                    
                    

                    	byte[] bytes = new byte[buffer.available()];
                    	System.out.println(bytes.length);
                    	buffer.read(bytes);
                    	String realData = new String(Encryption.decrypt(bytes));
                    	
                    	String[] masterdata = realData.split("\\{M\\}");
						String[] data = masterdata[1].split("\\{O\\}");
						System.out.println("Data from socket: "+realData+" "+masterdata[0]);
						
						for(int i=0;i< ChatScreen.chatrooms.size();i++)
						{
							// found the chat room for which to add data
							if(ChatScreen.chatrooms.get(i).room.equals(masterdata[0]))
							{
								System.out.println("iterating through stuff");
								for(int j=0 ;j< data.length ;j++)
								{
									ChatScreen.chatrooms.get(i).addMessage(data[j]);
									if(!ChatScreen.currChatroom.equals(ChatScreen.chatrooms.get(i)))
										ChatScreen.chatrooms.get(i).renderer.getDrawer("bg",NinepatchDrawer.class).setColor(Color.GREEN);
								}
							}
								
						}
						
                    } catch (Exception e) {
                    	System.out.println("error occured when trying to get data in socket: "+e.getMessage());
                    }
                }
            }
        }).start(); // And, start the thread running
        
        Gdx.app.addLifecycleListener(new LifecycleListener()
		{
			
			@Override
			public void resume()
			{
			
			}
			
			@Override
			public void pause()
			{
			}
			
			@Override
			public void dispose()
			{
				System.out.println("Called dispose");
				
				for(int i=0;i<chatrooms.size();i++)
				{
					HttpRequest exitRoom = new HttpRequest(HttpMethods.POST);
					exitRoom.setUrl("http://" + Servers.chat + "/exitChatroom");
					
					byte[] bytes = Encryption.encrypt(chatrooms.get(i).room.getBytes());
					ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
					exitRoom.setHeader("Content-Type", "text/plain");
					exitRoom.setContent(byteStream, bytes.length);
					Gdx.net.sendHttpRequest(exitRoom, new HttpResponseListener()
					{
						@Override
						public void failed(Throwable t)
						{
						}
						
						@Override
						public void cancelled()
						{
						}

						@Override
						public void handleHttpResponse(HttpResponse httpResponse)
						{
						}
					});
					
				};
				server.dispose();
				
			}
		});
	}
}
