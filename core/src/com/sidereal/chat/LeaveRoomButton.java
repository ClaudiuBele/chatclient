package com.sidereal.chat;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.Servers;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.behaviors.renderer.texture.TextureBuilder;
import com.sidereal.util.Notification;
import com.sidereal.utility.Encryption;
import com.sidereal.utility.ServerMessage;

public class LeaveRoomButton extends GameObject
{
	
	public Renderer renderer;
	public Clickable clickable;
	private Chatroom chatroom;

	public LeaveRoomButton(GameScene scene, Chatroom room)
	{

		super(scene, room);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Object... params)
	{
		chatroom = (Chatroom) params[0];
		
		renderer = new Renderer(this);
		
		TextureBuilder texture = new TextureBuilder("exit.png")
		.setSize(Gdx.graphics.getHeight()*0.09f, Gdx.graphics.getHeight()*0.09f)
		.setOffsetPosition(-Gdx.graphics.getHeight()*0.09f, 0);
		
		renderer.addDrawer("main", texture);
		
		clickable = new Clickable(this);
		clickable.setAreaSize(Gdx.graphics.getHeight()*0.09f, Gdx.graphics.getHeight()*0.09f, -Gdx.graphics.getHeight()*0.09f, 0);
		clickable.addActionEvent("UI", InputAction.FINGER_1, new ActionEvent()
		{
			public boolean run(ActionData inputData) 
			{
				
				
				HttpRequest exitRoom = new HttpRequest(HttpMethods.POST);
				exitRoom.setUrl("http://" + Servers.chat + "/exitChatroom");
				
				byte[] bytes = Encryption.encrypt(chatroom.room.getBytes());
				ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
				exitRoom.setHeader("Content-Type", "text/plain");
				exitRoom.setContent(byteStream, bytes.length);
				Gdx.net.sendHttpRequest(exitRoom, new HttpResponseListener()
				{
					@Override
					public void handleHttpResponse(HttpResponse httpResponse)
					{
						try
						{
							final String response = new String(Encryption.decrypt(httpResponse.getResult()), "UTF-8");
							
							// remove chatroom and update position of the other rooms
							
							
							if(ChatScreen.currChatroom.equals(chatroom)) ChatScreen.currChatroom = null;
							ChatScreen.chatrooms.remove(chatroom);
							for(int i=0;i<ChatScreen.chatrooms.size();i++)
							{
								ChatScreen.chatrooms.get(i).updatePosition();
							}
							scene.remove(chatroom.chat);
							scene.remove(chatroom);
							
							// if we removed the focused room and we have another room selected, set it to the focused one
							if(ChatScreen.chatrooms.size() > 0 && ChatScreen.currChatroom == null)
							{
								ChatScreen.currChatroom = ChatScreen.chatrooms.get(0);
								ChatScreen.currChatroom.renderer.getDrawer("bg", NinepatchDrawer.class).setColor(Color.YELLOW);
							}
							
							Notification.instance.add(ServerMessage.toString(Integer.parseInt(response)),new Color(Color.WHITE), true);
							
						} catch (UnsupportedEncodingException e)
						{
							e.printStackTrace();
						}

						
						
						
					}
					
					@Override
					public void failed(Throwable t)
					{
						// remove chatroom and update position of the other rooms
						Notification.instance.add("Unable to contact the chat server. "
							+ "Please check your internet connection and try again" , new Color(Color.WHITE), true);
					}
					
					@Override
					public void cancelled()
					{
						// remove chatroom and update position of the other rooms
						ChatScreen.chatrooms.remove(chatroom);
						for(int i=0;i<ChatScreen.chatrooms.size();i++)
						{
							ChatScreen.chatrooms.get(i).updatePosition();
						}
						scene.remove(chatroom);

					}
				});
				
				return true;
			};
		},
		InputEventType.Up, 
		true);
		super.onCreate(params);
	}

}
