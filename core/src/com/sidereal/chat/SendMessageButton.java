package com.sidereal.chat;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.Servers;
import com.sidereal.dolphinoes.architecture.AbstractEvent;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.events.EventTimer;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.util.LoadingSystem;
import com.sidereal.util.Notification;
import com.sidereal.utility.Encryption;

public class SendMessageButton extends GameObject
{
	private Renderer renderer;
	private Clickable clickable;
	private TextBuilder text;
	private ChatInputHandler input;
	@SuppressWarnings("unused")
	private EventTimer timer;
	
	public SendMessageButton(GameScene scene, ChatInputHandler input)
	{

		super(scene, input);
	}
	
	@Override
	protected void onCreate(Object... params)
	{
	
		timer = new EventTimer(this);
		input = (ChatInputHandler) params[0];
		
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();

		
		text = new TextBuilder(scene, false, "Calibri.fnt");
		text.setScale(smallestSize/2000f);
		text.setText("Send", Color.BLACK);
		text.setParent(this);
		
		renderer = new Renderer(this);
		
		NinepatchBuilder ninepatch = new NinepatchBuilder("button_9.png", 6, 6, 5, 11)
		.setSize(smallestSize*0.2f, smallestSize*0.08f)
		.setOffsetPosition(-smallestSize*0.1f, -smallestSize*0.04f);
		
		renderer.addDrawer("main", ninepatch);
		
		clickable = new Clickable(this);
		clickable.setAreaSize(smallestSize*0.2f, smallestSize*0.08f);
		
		ActionEvent runEvent = new ActionEvent()
		{

			@Override
			public boolean run(ActionData inputData)
			{
				if(ChatScreen.currChatroom == null || input.internalChatText.length() == 0)
				{
					return true;
				}
				LoadingSystem.instance.display(true);
				
				HttpRequest submitData = new HttpRequest(HttpMethods.POST);
				submitData.setUrl("http://" + Servers.chat + "/chatroom");
				
				
				byte[] bytes = Encryption.encrypt((ChatScreen.currChatroom.room+"{V}"+input.internalChatText).getBytes());
				ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
				submitData.setHeader("Content-Type", "text/plain");
				submitData.setContent(byteStream, bytes.length);
				
				Gdx.net.sendHttpRequest(submitData, new HttpResponseListener()
				{
					@Override
					public void handleHttpResponse(HttpResponse httpResponse)
					{
						try
						{
							final String response = new String(Encryption.decrypt(httpResponse.getResult()), "UTF-8");
							// length is one, must be a server message
							
							System.out.println("Response for sending data to a chat room: "+response);
							
							
							LoadingSystem.instance.display(false);
							instance.runOnGameThread(new AbstractEvent()
							{
								public  void run(Object... objects) {
									ChatScreen.currChatroom.addMessage("<"+Servers.nickname+"> : "+input.internalChatText,System.currentTimeMillis());
									input.reset();
								};
								
							});
							
						} catch (UnsupportedEncodingException e)
						{
							Notification.instance
							.add(
								"Unable to send message to the chat servers.\r\nTry again, if the problem persists, connect to another server",
								Color.WHITE, true);
							LoadingSystem.instance.display(false);
							
						}

						
					}
					
					@Override
					public void failed(Throwable t)
					{
					
						Notification.instance.add("Unable to contact chat server. Check your internet connection and try again in a moment", Color.WHITE, true);
						LoadingSystem.instance.display(false);

					}
					
					@Override
					public void cancelled()
					{
					}
				});
				
				// TODO Auto-generated method stub
				return super.run(inputData);
			}
		
		};
		
		
		clickable.addActionEvent("Main", InputAction.FINGER_1, runEvent, InputEventType.Up, true);
		
		// submit text on enter as well
		if(Gdx.app.getType().equals(ApplicationType.Desktop))
		{
			clickable.addActionEvent("Main", InputAction.KEY_ENTER, runEvent, InputEventType.Up, false);
		}
		
		
		super.onCreate(params);
	}
	
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = x > y ? y : x;
		
		renderer.getDrawer("main", NinepatchDrawer.class).setSize(x*0.12f, y*0.07f);
		renderer.getDrawer("main", NinepatchDrawer.class).setOffsetPosition(-x*0.06f, -y*0.035f);

		clickable.setAreaSize(x*0.12f, y*0.07f);
		
		text.setScale(smallestSize/2000f);
	}

}
