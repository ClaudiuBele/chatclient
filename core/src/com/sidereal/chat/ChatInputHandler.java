package com.sidereal.chat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.KeyTypedEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.dolphinoes.ui.TextBuilder.Allign;
import com.sidereal.login.BlackOverlay;

public class ChatInputHandler extends GameObject
{
	
	public String internalChatText;

	
	private Clickable clickable;
	private BlackOverlay overlay;
	private boolean pressing, selected;
	private TextBuilder text;
	
	private ActionEvent backspaceEvent;
	private KeyTypedEvent keyTypedEvent;
	
	public ChatInputHandler(GameScene scene, Object[] params)
	{

		super(scene, params);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	protected void onCreate(Object... params)
	{
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight() : Gdx.graphics.getWidth();

		internalChatText = "";
		text = new TextBuilder(scene, true,"Calibri.fnt");
		text.setScale(smallestSize / 2200f);
		text.setText("Enter message here",Color.BLACK);
		text.setParent(this);
		text.position.setLocal(0, 0);
		text.setAllign(Allign.Left);
		
		overlay = BlackOverlay.instance;
		clickable = new Clickable(this);
		// make area bigger for phone users
		
		float width = Gdx.graphics.getWidth()*0.9f;
		float height = Gdx.graphics.getHeight()*0.07f;
		
		clickable.setAreaSize(width, height);

		ActionEvent pressEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{

				pressing = true;
				return false;
			}
		};
		ActionEvent releaseEvent = new ActionEvent()
		{
			
			@Override
			public boolean run(ActionData inputData)
			{

				if(pressing && overlay.isEnabled() == false)
				{
					selected = true;
					Gdx.input.setOnscreenKeyboardVisible(true);
					if(!Gdx.app.getType().equals(ApplicationType.Desktop))
					{
						overlay.setEnabled(true);
						overlay.setText("Message to send to chat room", internalChatText);

					}
					return false;
				}
				return false;
			}
		};
		ActionEvent outsideEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
				

				if(selected)
				{
					Gdx.input.setOnscreenKeyboardVisible(false);
					overlay.setEnabled(false);
				}
				

				selected = false;
				
				return super.run(inputData);
			}
		};
		backspaceEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{			

				if(internalChatText.length() > 0)
				{
					internalChatText = internalChatText.substring(0, internalChatText.length()-1);
					if(internalChatText.length() == 0)
						text.setText("Enter message here", Color.BLACK );
					else
						text.setText(internalChatText, Color.BLACK);

					overlay.setText("Message to send to chat room", internalChatText);

				}
				return false;
			}
		};
		keyTypedEvent = new KeyTypedEvent()
		{
			final String availableKeys = "abcdefghijklmnopqrstuvxyzwABCDEFGHIJKLMNOPQRSTUVXYZW 1234567890.:";
			@Override
			public boolean run(char character)
			{
				if(selected && availableKeys.indexOf(character) != -1)
				{
					// key is a proper key
					if(internalChatText.length()  < 100 )
					{
						internalChatText+= character;
						text.setText(internalChatText, Color.BLACK);
						overlay.setText("Message to send to chat room", internalChatText);

						return false;
					}
				}
				return false;
			}
		};
		
		
		clickable.addActionEvent("Main", InputAction.FINGER_1, pressEvent , InputEventType.Down, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, releaseEvent, InputEventType.Up, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, outsideEvent, InputEventType.Up, false);
		DolphinOES.input.addActionEvent("Main", InputAction.KEY_BACKSPACE,backspaceEvent, InputEventType.Down);
		DolphinOES.input.addKeyTypedEvent("Main", keyTypedEvent);
		
		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);
	}

	public void reset()
	{
		internalChatText = "";
		text.setText("Enter message here", Color.BLACK);
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = x > y ? y : x ;

		position.set(x/2f, y*.1f - y*0.035f);
		
		clickable.setAreaSize(x*0.9f, y*0.07f);
		
		text.setScale(smallestSize/3000f);
		text.position.setLocal(- x*0.435f, 0);
	}
}
