package com.sidereal.chat;

import java.util.ArrayList;
import java.util.List;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.behaviors.renderer.texture.TextureDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.dolphinoes.ui.TextBuilder.Allign;
import com.sidereal.dolphinoes.ui.TextBuilder.Anchor;

public class Chatroom extends GameObject
{
	
	private static class ChatroomMessage{
		
		public String data;
		public long timestamp;
		
		public ChatroomMessage(String data, long timestamp)
		{
			this.data = data;
			this.timestamp = timestamp;
		}
	}
	
	public String room;
	private TextBuilder text;
	
	
	TextBuilder chat;
	private List<ChatroomMessage> chatMessages;
	
	
	Renderer renderer;
	private Clickable clickable;
	
	private LeaveRoomButton exit;
	
	public Chatroom(GameScene scene, String name)
	{
		super(scene, name);
	}
	
	
	@Override
	protected void onUpdate()
	{
		if(chat.bounds.y > Gdx.graphics.getHeight() * 0.65f && chatMessages.size()>0)
		{
			chat.clearText();
			chatMessages.remove(0);				
			for(int i=0 ; i< chatMessages.size();i++)
			{
				chat.addText(chatMessages.get(i).data, Color.BLACK);
			}
		}
		boolean removedMessage = false;

		while(chatMessages.size() > 0)
		{
			// 5 minutes have passed since message has been added
			
			if( System.currentTimeMillis() -  chatMessages.get(0).timestamp > 1000 * 60 * 5)
			{
				removedMessage = true;
				chatMessages.remove(0);				
			}
			else
			{
				if(removedMessage)
				{
					chat.clearText();
					for(int i=0 ; i< chatMessages.size();i++)
					{
						chat.addText(chatMessages.get(i).data, Color.BLACK);
					}
				}
				return;
			}
		}
	}
	
	@Override
	protected void onCreate(Object... params)
	{
		Color.WHITE.set(1, 1, 1, 1);
		room = (String) params[0];
		
		chatMessages = new ArrayList<Chatroom.ChatroomMessage>();
		
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();
		
		float width = Gdx.graphics.getWidth()*0.9f / 5f;
		float height = Gdx.graphics.getHeight()*0.07f;

		renderer = new Renderer(this);
		clickable = new Clickable(this);
		clickable.setAreaSize(width, height);
		clickable.addActionEvent("Main", InputAction.FINGER_1, new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
				if(ChatScreen.currChatroom != null)
				{
					ChatScreen.currChatroom.renderer.getDrawer("bg",NinepatchDrawer.class).setColor(new Color(1,1,1,1));
					ChatScreen.currChatroom.chat.setEnabled(false);
				}
				
				ChatScreen.currChatroom = (Chatroom) instance;
				ChatScreen.currChatroom.renderer.getDrawer("bg",NinepatchDrawer.class).setColor(Color.YELLOW);

				ChatScreen.currChatroom.chat.setEnabled(true);
				
				return false;
				
			}
		}, InputEventType.Up, true	);
		
		
		NinepatchBuilder bg = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(width, height)
		.setOffsetPosition(-width/2f, -height/2f)
		.setScale(2, 2);

		renderer.addDrawer("bg", bg);

		chat = new TextBuilder(scene, true, "Calibri.fnt");
		chat.setAllign(Allign.Left);
		chat.setAnchor(Anchor.Top);
		chat.setScale(smallestSize/2000f);
		chat.setWindowSize(Gdx.graphics.getWidth()*0.9f - smallestSize*0.3f);
		chat.position.set(Gdx.graphics.getWidth()*0.05f	, Gdx.graphics.getHeight()*0.8f);
		chat.setEnabled(true);
		
		text = new TextBuilder(scene, false, "Calibri.fnt");
		text.setParent(this);		
		text.setAllign(Allign.Left);		
		text.position.setLocalX(-width*0.45f);		
		text.position.setLocalZ(1);		
		text.setText(room, Color.BLACK);
		text.setScale(smallestSize/2500f);


		exit = new LeaveRoomButton(scene, this);
		exit.setParent(this);
		exit.position.setLocalZ(1);
		exit.position.setLocal(width/2f - height*0.55f, 0);

		super.onCreate(params);
	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = x > y ? y : x;

		float width = x *0.9f / 5f;
		float height = y *0.07f;
		
		text.setScale(smallestSize/2500f);
		text.position.setLocalX(-width*0.45f);
		
		chat.setScale(smallestSize/3000f);
		chat.setWindowSize(Gdx.graphics.getWidth()*0.9f - smallestSize*0.3f);
		chat.position.set(Gdx.graphics.getWidth()*0.07f	, Gdx.graphics.getHeight()*0.78f);
		
		
		renderer.getDrawer("bg", NinepatchDrawer.class).setSize(width, height);
		renderer.getDrawer("bg", NinepatchDrawer.class).setOffsetPosition(-width/2f, -height/2f);
		
		
		if(ChatScreen.chatrooms.indexOf(this) != -1)
		{
			position.set(x*0.05f + width * ChatScreen.chatrooms.indexOf(this) + width/2f, y*0.835f);
		}
		exit.position.setLocal(width/2f - height*0.3f, 0);

		exit.renderer.getDrawer("main", TextureDrawer.class).setSize(new Vector2(height*0.5f, height*0.5f));
		exit.renderer.getDrawer("main", TextureDrawer.class).setOffsetPosition(new Vector2(-height*0.25f, -height*0.25f));
		
		exit.clickable.setAreaSize(height*0.5f, height*0.5f);
		exit.position.setLocal(width/2f - height*0.55f, 0);

		clickable.setAreaSize(width,height);
		
	}
	
	public void updatePosition()
	{
		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);
	}
	
	public void addMessage(String message, long timestamp)
	{
		chatMessages.add(new ChatroomMessage(message, timestamp));
		chat.addText(message, Color.BLACK);
	}
	
	public void addMessage(String fullData)
	{
		String[] objectData = fullData.split("\\{V\\}");
		addMessage(objectData[1], Long.parseLong(objectData[0]));
	}

}
