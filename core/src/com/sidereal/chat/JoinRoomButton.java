package com.sidereal.chat;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.Servers;
import com.sidereal.dolphinoes.architecture.AbstractEvent;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.events.EventTimer;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.util.LoadingSystem;
import com.sidereal.util.Notification;
import com.sidereal.utility.Encryption;
import com.sidereal.utility.ServerMessage;

public class JoinRoomButton extends GameObject
{
	private Renderer renderer;
	private Clickable clickable;
	TextBuilder text;
	private ChatroomHandler chatroom;
	private EventTimer timer;

	public JoinRoomButton(GameScene scene, ChatroomHandler chatroom)
	{

		super(scene, chatroom);
	}

	@Override
	protected void onCreate(Object... params)
	{

		timer = new EventTimer(this);
		chatroom = (ChatroomHandler) params[0];

		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();

		text = new TextBuilder(scene, false, "Calibri.fnt");
		text.setScale(smallestSize / 2000f);
		text.setText("Join room", Color.BLACK);
		text.setParent(this);

		renderer = new Renderer(this);

		NinepatchBuilder ninepatch = new NinepatchBuilder("button_9.png", 6, 6, 5, 11).setSize(smallestSize * 0.2f,
			smallestSize * 0.08f).setOffsetPosition(-smallestSize * 0.1f, -smallestSize * 0.04f);

		renderer.addDrawer("main", ninepatch);

		clickable = new Clickable(this);
		clickable.setAreaSize(smallestSize * 0.2f, smallestSize * 0.08f);
		clickable.addActionEvent("Main", InputAction.FINGER_1, new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{

				// empty input
				if (chatroom.internalChatroomName.length() == 0)
					return false;

				if (ChatScreen.chatrooms.size() >= 5)
				{
					Notification.instance.add("You must leave a chat room before you can join another one.",
						Color.WHITE, true);
					return true;
				}

				HttpRequest joinRoom = new HttpRequest(HttpMethods.POST);
				joinRoom.setUrl("http://" + Servers.chat + "/joinchatroom");
				joinRoom.setTimeOut(500);

				byte[] bytes = Encryption.encrypt(chatroom.internalChatroomName.getBytes());
				ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
				joinRoom.setHeader("Content-Type", "text/plain");
				joinRoom.setContent(byteStream, bytes.length);

				LoadingSystem.instance.display(true);
				
				Gdx.net.sendHttpRequest(joinRoom, new HttpResponseListener()
				{

					@Override
					public void handleHttpResponse(HttpResponse httpResponse)
					{

						try
						{
							final String response = new String(Encryption.decrypt(httpResponse.getResult()), "UTF-8");
							// length is one, must be a server message

							System.out.println("Response for joining a chat room: " + response);

							// we didn't get any data along with the request for joining the map
							if (!response.contains("{M}"))
							{
								// detect if we already have the chatroom connect to
								// in order for the room to be added in case the message is
								// CHATROOM_ALREADY_JOINED ( this might be returned and we might not
								// have it if we exit app before reconnecting
								boolean foundRoom = false;
								for (int i = 0; i < ChatScreen.chatrooms.size(); i++)
								{
									if (ChatScreen.chatrooms.get(i).room.equals(chatroom.internalChatroomName
										.toLowerCase()))
										foundRoom = true;
								}

								int data = Integer.parseInt(response);
								if (data == ServerMessage.CHATROOM_ALREADY_JOINED && !foundRoom)
									data = ServerMessage.CHATROOM_ENTERED;

								Notification.instance.add(ServerMessage.toString(data), Color.WHITE, true);

								// might be CLIENT_NOT_AUTHENTICATED, CHATROOM_FULL that we don't want
								// to make rooms for.
								System.out.println((data != ServerMessage.CHATROOM_CREATED) + "  "
									+ (data != ServerMessage.CHATROOM_ENTERED));
								if (data != ServerMessage.CHATROOM_CREATED && data != ServerMessage.CHATROOM_ENTERED)
									return;

								System.out.println("Passed check");

								timer.setEvent(0.1f, new AbstractEvent()
								{
									public void run(Object... objects)
									{

										System.out.println("In timer event");

										Chatroom chat = new Chatroom(scene, chatroom.internalChatroomName.toLowerCase());
										ChatScreen.chatrooms.add(chat);
										chat.updatePosition();

										chatroom.internalChatroomName = "";
										chatroom.inputText.setText("Enter name", Color.BLACK);

										if (ChatScreen.currChatroom == null)
										{
											ChatScreen.currChatroom = chat;
											chat.renderer.getDrawer("bg", NinepatchDrawer.class).setColor(Color.YELLOW);
										}
										else
										{
											chat.chat.setEnabled(false);
										}
										System.out.println("Entered chatroom");
									};
								});

							}
							// server exists, we join
							else
							{
								timer.setEvent(0.1f, new AbstractEvent()
								{

									public void run(Object... objects)
									{

										for (int i = 0; i < ChatScreen.chatrooms.size(); i++)
										{
											if (ChatScreen.chatrooms.get(i).room.equals(chatroom.internalChatroomName))
												return;
										}

										Chatroom chat = new Chatroom(scene, chatroom.internalChatroomName.toLowerCase());
										ChatScreen.chatrooms.add(chat);
										chat.updatePosition();

										chatroom.internalChatroomName = "";
										chatroom.inputText.setText("Enter name", Color.BLACK);

										Notification.instance.add("You have succesfully entered the chat room",
											Color.WHITE, true);

										
										if (ChatScreen.currChatroom == null)
										{
											ChatScreen.currChatroom = chat;
											chat.renderer.getDrawer("bg", NinepatchDrawer.class).setColor(Color.YELLOW);
										}
										else
										{
											chat.chat.setEnabled(false);
										}
										
										String[] masterdata = response.split("\\{M\\}");
										if(masterdata.length == 1) return;
										String[] data = masterdata[1].split("\\{O\\}");

										for (int i = 0; i < data.length; i++)
										{
											chat.addMessage(data[i]);
										}
									}

								});

							}

							
							
						} catch (UnsupportedEncodingException e)
						{
							Notification.instance.add(
								"Unable to contact chat server. Check your internet connection and try again in a moment",
								Color.WHITE, true);
						}
						LoadingSystem.instance.display(false);


					}

					@Override
					public void failed(Throwable t)
					{

						Notification.instance.add(
							"Unable to contact chat server. Check your internet connection and try again in a moment",
							Color.WHITE, true);
						LoadingSystem.instance.display(false);


					}

					@Override
					public void cancelled()
					{

					}
				});

				return super.run(inputData);
			}
		}, InputEventType.Up, true);

		// TODO Auto-generated method stub
		super.onCreate(params);
	}

	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{

		float smallestSize = x > y ? y : x;

		renderer.getDrawer("main", NinepatchDrawer.class).setSize(x * 0.12f, y * 0.07f);
		renderer.getDrawer("main", NinepatchDrawer.class).setOffsetPosition(-x * 0.06f, -y * 0.035f);

		clickable.setAreaSize(x * 0.12f, y * 0.07f);

		text.setScale(smallestSize / 2000f);
	}

}
