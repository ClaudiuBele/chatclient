package com.sidereal.chat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.architecture.core.input.ActionData;
import com.sidereal.dolphinoes.architecture.core.input.ActionEvent;
import com.sidereal.dolphinoes.architecture.core.input.KeyTypedEvent;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputAction;
import com.sidereal.dolphinoes.architecture.core.input.Input.InputEventType;
import com.sidereal.dolphinoes.behaviors.input.Clickable;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;
import com.sidereal.dolphinoes.ui.TextBuilder;
import com.sidereal.dolphinoes.ui.TextBuilder.Anchor;
import com.sidereal.login.BlackOverlay;

public class ChatroomHandler extends GameObject
{
	TextBuilder inputText;
	private TextBuilder description;
	private TextBuilder submit;
	public String internalChatroomName;
	
	private Renderer renderer;
	private Clickable clickable;
	
	private BlackOverlay overlay;

	/** Used for detection of pressing the input
	 */
	private boolean pressing, selected;
	
	/** Events for handling typed keys for room name
	 */
	private KeyTypedEvent keyTypedEvent;
	private ActionEvent backspaceEvent;
	
	private JoinRoomButton button;
	private SendMessageButton submitButton;
	
	public ChatroomHandler(GameScene scene)
	{
		super(scene);
	}
	
	
	@Override
	protected void onCreate(Object... params)
	{
		overlay = BlackOverlay.instance;
		
		internalChatroomName = "";
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();
		
		renderer = new Renderer(this);
		clickable = new Clickable(this);
		clickable.setAreaSize(smallestSize*0.2f, smallestSize*0.08f, - smallestSize*0.1f,  -smallestSize *0.05f);
		
		//region clickable events
		
		ActionEvent pressEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{

				pressing = true;
				return false;
			}
		};
		ActionEvent releaseEvent = new ActionEvent()
		{
			
			@Override
			public boolean run(ActionData inputData)
			{

				if(pressing  && overlay.isEnabled() == false)
				{
					selected = true;
					Gdx.input.setOnscreenKeyboardVisible(true);
					if(!Gdx.app.getType().equals(ApplicationType.Desktop))
					{
						overlay.setEnabled(true);
						overlay.setText("Enter chat room name below", internalChatroomName);

					}
					return false;
				}
				return false;
			}
		};
		ActionEvent outsideEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{
				

				if(selected)
				{
					Gdx.input.setOnscreenKeyboardVisible(false);
					overlay.setEnabled(false);
				}
				
				selected = false;
				
				return super.run(inputData);
			}
		};
		backspaceEvent = new ActionEvent()
		{
			@Override
			public boolean run(ActionData inputData)
			{			

				if(internalChatroomName.length() > 0)
				{
					internalChatroomName = internalChatroomName.substring(0, internalChatroomName.length()-1);
					if(internalChatroomName.length() == 0)
						inputText.setText("Enter name here", Color.BLACK);
					else
						inputText.setText(internalChatroomName,  Color.BLACK);

					overlay.setText("Enter chat room name below", internalChatroomName);

				}
				return false;
			}
		};
		keyTypedEvent = new KeyTypedEvent()
		{
			final String availableKeys = "abcdefghijklmnopqrstuvxyzwABCDEFGHIJKLMNOPQRSTUVXYZW 1234567890.:";
			@Override
			public boolean run(char character)
			{
				if(selected)
				{
					// key is a proper key
					if(availableKeys.indexOf((int)character) != -1  && internalChatroomName.length() <= 12)
					{
						internalChatroomName+= character;
						inputText.setText(internalChatroomName, Color.BLACK);
						overlay.setText("Enter chat room name below", internalChatroomName);

						return false;
					}
				}
				return false;
			}
		};
		clickable.addActionEvent("Main", InputAction.FINGER_1, pressEvent , InputEventType.Down, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, releaseEvent, InputEventType.Up, true);
		clickable.addActionEvent("Main", InputAction.FINGER_1, outsideEvent, InputEventType.Up, false);
		DolphinOES.input.addActionEvent("Main", InputAction.KEY_BACKSPACE,backspaceEvent, InputEventType.Down);
		DolphinOES.input.addKeyTypedEvent("Main", keyTypedEvent);
		
		//endregion
		
		NinepatchBuilder textInput = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(smallestSize*0.2f, smallestSize*0.08f)
		.setOffsetPosition(- smallestSize*0.1f,  -smallestSize *0.05f);
	
		NinepatchBuilder bg = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(smallestSize*0.25f, smallestSize*0.35f)
		.setOffsetPosition(- smallestSize*0.125f, -smallestSize *0.175f)
		.setScale(2, 2);
		
		renderer.addDrawer("bg", bg);
		renderer.addDrawer("input", textInput);
		
		inputText = new TextBuilder(scene, false, "Calibri.fnt");
		inputText.setParent(this);
		inputText.position.setLocalZ(2);
		inputText.setScale(smallestSize/3000f);
		inputText.setText("Enter name",Color.BLACK);
		inputText.position.setLocal(0, -smallestSize * 0.01f);

		description = new TextBuilder(scene, true, "Calibri.fnt");
		description.setParent(this);
		description.setScale(smallestSize/2000f);
		description.position.setLocalZ(1);
		description.setText("Join chat room", Color.BLACK);
		description.position.setLocal(0, smallestSize*0.15f);
		description.setWindowSize(smallestSize*0.22f);
		description.setAnchor(Anchor.Top);

		
		submit = new TextBuilder(scene, true, "Calibri.fnt");
		submit.setParent(this);
		submit.setScale(smallestSize/2000f);
		submit.position.setLocalZ(1);
		submit.setText("Submit message", Color.BLACK);
		submit.position.setLocal(0, smallestSize*0.15f);
		submit.setWindowSize(smallestSize*0.22f);
		submit.setAnchor(Anchor.Top);
		
		submitButton = new SendMessageButton(scene, new ChatInputHandler(scene, null));
		submitButton.position.setLocal(0, -smallestSize*0.35f);
		submitButton.setParent(this);
		submitButton.position.setLocalZ(1);
		
		button = new JoinRoomButton(scene, this);
		button.position.setLocal(0, -smallestSize*0.12f);
		button.setParent(this);
		button.position.setLocalZ(1);
		
		position.set(Gdx.graphics.getWidth() - smallestSize* 0.3f, Gdx.graphics.getHeight()/2f);
		
		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);

	}
	
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
		float smallestSize = x > y ? y : x;

		clickable.setAreaSize(x*0.12f, y*0.08f,- x*0.06f,  y *0.07f);

		renderer.getDrawer("bg", NinepatchDrawer.class).setSize(x*0.15f, y*0.7f);
		renderer.getDrawer("bg", NinepatchDrawer.class).setOffsetPosition(- x*0.0775f, -y *0.4f);

		renderer.getDrawer("input", NinepatchDrawer.class).setSize(x*0.12f, y*0.08f);
		renderer.getDrawer("input", NinepatchDrawer.class).setOffsetPosition(- x*0.06f,  y *0.07f);
		
		inputText.position.setLocal(0, y * 0.11f);
		inputText.setScale(smallestSize/3000f);
		
		description.position.setLocal(0, y*0.25f);
		description.setWindowSize(smallestSize*0.2f);
		description.setScale(smallestSize/2000f);

		submit.position.setLocal(0, - y *0.125f);
		submit.setWindowSize(smallestSize*0.2f);
		submit.setScale(smallestSize/2000f);

		
		button.position.setLocal(0,  0);
		submitButton.position.setLocal(0, -y * 0.275f);

		position.set(x*0.875f, y/2f);
		
	}

}
