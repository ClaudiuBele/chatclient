package com.sidereal.chat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.ninepatch.NinepatchDrawer;

public class ChatFrame extends GameObject
{

	private Renderer renderer;
	
	public ChatFrame(GameScene scene, Object[] params)
	{
		super(scene, params);
		
	}
	
	@Override
	protected void onCreate(Object... params)
	{
		renderer = new Renderer(this);
		
		NinepatchBuilder tabs = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(Gdx.graphics.getWidth()*0.9f, Gdx.graphics.getHeight()*0.07f)
		.setOffsetPosition(0, Gdx.graphics.getHeight()*0.3f)
		.setScale(2, 2)
				.setColor(new Color(1,1,1,0.55f));
		
		NinepatchBuilder bg = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.7f)
		.setOffsetPosition(0,- Gdx.graphics.getHeight()*0.4f)
		.setScale(2, 2)
		.setColor(new Color(1,1,1,0.55f));
		
		NinepatchBuilder input = new NinepatchBuilder("background_9.png", 6, 6, 6, 6)
		.setSize(Gdx.graphics.getWidth()*0.9f, Gdx.graphics.getHeight()*0.07f)
		.setOffsetPosition(0, -Gdx.graphics.getHeight()*0.47f)
		.setScale(2, 2)
		.setColor(new Color(1,1,1,0.55f));
		
		position.set(Gdx.graphics.getWidth()*0.05f,  Gdx.graphics.getHeight()/2f);
		position.setZ(-1);
		
		renderer.addDrawer("bg", bg);
		renderer.addDrawer("tabs", tabs);
		renderer.addDrawer("input", input);

	}
	
	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{
	
		renderer.getDrawer("tabs",NinepatchDrawer.class).setSize(x * 0.9f,  y * 0.07f);
		renderer.getDrawer("tabs",NinepatchDrawer.class).setOffsetPosition(0, y * 0.3f);

		renderer.getDrawer("bg",NinepatchDrawer.class).setSize(x * 0.75f,  y * 0.7f);
		renderer.getDrawer("bg",NinepatchDrawer.class).setOffsetPosition(0, - y * 0.4f);

		renderer.getDrawer("input",NinepatchDrawer.class).setSize(x * 0.9f,  y * 0.07f);
		renderer.getDrawer("input",NinepatchDrawer.class).setOffsetPosition(0, - y * 0.47f);
		
		
		position.set(x*0.05f, y/2f);
		super.onResize(x, y, oldX, oldY);
	}

}
