package com.sidereal.util;

import com.badlogic.gdx.Gdx;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.ui.MessageBubble;

public class Notification extends MessageBubble
{

	public static Notification instance;
	
	public Notification(GameScene scene)
	{
		super("mb", scene, ShrinkType.AlphaChannel, "Overlay");
		
		isPersistent = true;
		
		instance = this;
		float smallestSize = Gdx.graphics.getWidth() > Gdx.graphics.getHeight() ? Gdx.graphics.getHeight()
			: Gdx.graphics.getWidth();
		
		setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		setColor(0.1f, 0.1f, 0.1f, 0.9f);
		position.set(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f);
		text.setFont("Calibri.fnt");
		text.setScale(smallestSize / 2000f);
		text.setWindowSize(Gdx.graphics.getWidth() * 0.8f);
	}
	
	@Override
	public void onResize(float x, float y, float oldX, float oldY)
	{
	
		super.onResize(x, y, oldX, oldY);
		text.setWindowSize(Gdx.graphics.getWidth() * 0.8f);

	}

}
