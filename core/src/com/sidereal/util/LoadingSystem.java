package com.sidereal.util;

import java.lang.ref.WeakReference;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.GameObject;
import com.sidereal.dolphinoes.architecture.GameScene;
import com.sidereal.dolphinoes.behaviors.events.EventTimer;
import com.sidereal.dolphinoes.behaviors.renderer.Renderer;
import com.sidereal.dolphinoes.behaviors.renderer.sprite.SpriteBuilder;
import com.sidereal.dolphinoes.behaviors.renderer.sprite.SpriteDrawer;
import com.sidereal.dolphinoes.util.Utility;

public class LoadingSystem extends GameObject
{

	private float internalTransparency;

	private final float enabledTransparency = 0.7f;
	private final float disabledTransparency = 0f;

	private Color color;

	private boolean displayed;

	private Renderer renderer;
	public EventTimer timer;

	private float rotation;

	public static LoadingSystem instance;

	public LoadingSystem(GameScene scene, Object[] params)
	{

		super(scene, params);
		instance = this;
	}

	@Override
	protected void onCreate(Object... params)
	{

		setName("Loading");
		setType("UI");

		displayed = false;
		timer = new EventTimer(this);
		renderer = new Renderer(this);
		color = new Color(0, 0, 0, 0.9f);
		internalTransparency = 0.1f;

		// making the spinner
		// signal GC he can collect the builder
		WeakReference<SpriteBuilder> texture = new WeakReference<SpriteBuilder>(new SpriteBuilder("spinner.png"));
		renderer.addDrawer("Spinner", texture.get());

		// make background behind spinner
		// signal GC he can collect the builder
		WeakReference<SpriteBuilder> bg = new WeakReference<SpriteBuilder>(new SpriteBuilder(
			DolphinOES.assets.frameworkAssetsFolder + "White.png"));
		renderer.addDrawer("BG", bg.get());
		renderer.placeAtStart("BG");

		onResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0);

		isPersistent = true;
	}

	public void display(boolean enable)
	{

		displayed = enable;
	}
	
	public boolean displayed()
	{
		return displayed;
	}

	@Override
	protected void onUpdate()
	{

		// interpolate alpha channel value based on whether or not
		// the system is enabled

		float prevTransparency = internalTransparency;

		internalTransparency = Utility.lerpTowards(internalTransparency, (displayed) ? enabledTransparency
			: disabledTransparency, 10f * DolphinOES.time.getDeltaTime());

		if (internalTransparency != 0)
		{
			renderer.getDrawer("Spinner", SpriteDrawer.class).setRotation(rotation, false);
			rotation = (rotation + 360 * DolphinOES.time.getDeltaTime()) % 360;
		}

		if (internalTransparency == prevTransparency)
			return;

		color.a = internalTransparency;
		renderer.getDrawer("BG", SpriteDrawer.class).setColor(color);
		renderer.getDrawer("Spinner", SpriteDrawer.class).setColor(color);

	}

	@Override
	protected void onResize(float x, float y, float oldX, float oldY)
	{

		float smallestSize = x > y ? y : x;

		renderer.getDrawer("Spinner", SpriteDrawer.class).setSizeAndCenter(smallestSize * 0.4f, smallestSize * 0.4f)
			.setOrigin(smallestSize * 0.2f, smallestSize * 0.2f);

		renderer.getDrawer("BG", SpriteDrawer.class).setSizeAndCenter(x, y);

		position.set(x / 2f, y / 2f, 10);

		super.onResize(x, y, oldX, oldY);
	}

}
