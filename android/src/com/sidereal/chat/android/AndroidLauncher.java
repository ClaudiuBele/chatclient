package com.sidereal.chat.android;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.core.DolphinOESConfiguration;
import com.sidereal.splash.SplashScreen;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		
		DolphinOESConfiguration dconfig = new DolphinOESConfiguration();
		dconfig.inputProcessorNames = new String[]{"Overlay","UI","Main"};
		dconfig.debugEnabled = true;
		
		initialize(new DolphinOES(new SplashScreen(),dconfig), config);
	}
}
