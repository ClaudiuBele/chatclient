package com.sidereal.chat.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.sidereal.chat.MyChatGame;
import com.sidereal.dolphinoes.architecture.DolphinOES;
import com.sidereal.dolphinoes.architecture.core.DolphinOESConfiguration;
import com.sidereal.splash.SplashScreen;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(480, 320);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                
                DolphinOESConfiguration dconfig = new DolphinOESConfiguration();
        		dconfig.inputProcessorNames = new String[]{"Overlay","UI","Main"};
        		dconfig.debugEnabled = true;
        		
        		return new DolphinOES(new SplashScreen(),dconfig);
        }
}